import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import vueCountryRegionSelect from 'vue-country-region-select';
import CountryFlag from 'vue-country-flag';
import VuePhoneNumberInput from 'vue-phone-number-input';
import 'vue-phone-number-input/dist/vue-phone-number-input.css';
import axios from 'axios';
import { getUser } from './StorageFactory';
import { environment } from './config';
import JsonCSV from 'vue-json-csv';
import DatetimePicker from 'vuetify-datetime-picker'
import socket from './service/socket'

Vue.config.productionTip = false
Vue.use(vueCountryRegionSelect);
Vue.component('country-flag', CountryFlag);
Vue.component('VuePhoneNumberInput', VuePhoneNumberInput);
Vue.component('downloadCsv', JsonCSV);
Vue.use(DatetimePicker)


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
  created() {
    const user = getUser();
    // console.log('user', user);
    if (user) {
      this.$store.commit('SET_USER_DATA', user);
      axios.defaults.headers.common['Authorization'] = `JWT ${ user?.token }`;
    }
    axios.defaults.baseURL = `${environment.apiEndPoint}v1/`;
    axios.interceptors.response.use(
      response => response,
      error => {
        if (error.response.status === 401 && ['token-expired', 'invalid-user'].includes(error.response.data.message)) {
          this.$store.dispatch('logout')
        }
        return Promise.reject(error)
      }
    );
  },
  mounted() {
    const user = this.$store.getters.user
    if(user) {
      socket.emit('admin-join', { jwt: user?.token })
      socket.on('reconnect', () => {
        socket.emit('admin-join', { jwt: user?.token })
      })
    }
  }
}).$mount('#app')

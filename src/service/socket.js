import { io } from "socket.io-client";
import { environment } from "../config";

export default io(environment.apiEndPoint);
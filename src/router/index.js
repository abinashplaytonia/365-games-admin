import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store';

Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      redirect: '/dashboard'
    },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
    meta: { layout: 'no-sidebar'},
  },
  {
    path: '/contact',
    name: 'ContactUs',
    component: () => import('@/views/ContactUs.vue')
  },
  {
    path: '/admins',
    name: 'Admins',
    component: () => import('@/views/Admins.vue')
  },
  {
    path: '/gallery',
    name: 'Gallery',
    component: () => import('@/views/Gallery.vue')
  },
  {
    path: '/users',
    name: 'User',
    component: () => import('@/views/User.vue')
  },
  {
    path: '/rewards',
    name: 'Rewards',
    component: () => import('@/views/Rewards.vue')
  },
  {
    path: '/teams',
    name: 'Team',
    component: () => import('@/views/Teams/Teams.vue')
  },
  {
    path: '/tournaments',
    name: 'Tournaments',
    component: () => import('@/views/Tournaments.vue')
  },
  {
    path: '/tournament/create',
    name: 'CreateTournament',
    component: () => import('@/components/AddEditTournament.vue')
  },
  {
    path: '/tournament/update/:id',
    name: 'UpdateTournament',
    component: () => import('@/components/AddEditTournament.vue')
  },
  {
    path: '/leaderboards',
    name: 'Leaderboards',
    component: () => import('@/views/Leaderboards.vue')
  },
  {
    path: '/leaderboard/create',
    name: 'CreateLeaderboard',
    component: () => import('@/components/AddEditLeaderboard.vue')
  },
  {
    path: '/leaderboard/update/:id',
    name: 'UpdateLeaderboard',
    component: () => import('@/components/AddEditLeaderboard.vue')
  },
  {
    path: '/subscription',
    name: 'Subscription',
    component: () => import('@/views/Subscription.vue')
  },
  {
    path: '/slider',
    name: 'Slider',
    component: () => import('@/views/Sliders.vue')
  },
  {
    path: '/ads',
    name: 'Ads',
    component: () => import('@/views/Ads.vue')
  },
  {
    path: '/faqs',
    name: 'Faqs',
    component: () => import('@/views/Faqs.vue')
  },
  {
    path: '/game',
    name: 'Game',
    component: () => import('@/views/Game.vue')
  },
  {
    path: '/redeem',
    name: 'Products',
    component: () => import('@/views/Products.vue')
  },
  {
    path: '/orders',
    name: 'Orders',
    component: () => import('@/views/Orders.vue')
  },
  {
    path: '/order/details',
    name: 'OrderDetails',
    component: () => import('@/components/OrderDetails.vue')
  },
  {
    path: '/taskstatus',
    name: 'TaskStatus',
    component: () => import('@/views/TaskStatus.vue')
  },
  {
    path: '/random-drop',
    name: 'RandomDrop',
    component: () => import('@/views/RandomDrop.vue')
  },
  {
    path: '/random-drop/prize/:id',
    name: 'RandomdropPrizes',
    component: () => import('@/views/RandomdropPrizes.vue')
  },
  {
    path: '/random-drop/participants/:id',
    name: 'RandomdropParticipants',
    component: () => import('@/views/RandomdropParticipants.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('@/views/Dashboard.vue')
  },
  {
    path: '/roles',
    name: 'Roles',
    component: () => import('@/views/Roles.vue')
  },
  {
    path: '/approval',
    name: 'Approval',
    component: () => import('@/views/Approval.vue')
  },
  // {
  //   path: '/forums',
  //   name: 'Forums',
  //   component: () => import('@/views/Forums.vue')
  // },
  {
    path:'/support',
    name:'Support',
    component: () => import('@/views/Support.vue')
  },
  {
    path: '/user',
    name: 'UserDetail',
    component: () => import(/* webpackChunkName: "UserSummary" */ '@/views/UserSummary.vue'),
    redirect: { name: 'UserSummary' },
    children: [
      {
        path: 'referral-summary',
        name: 'ReferralSummary',
        component: () => import(/* webpackChunkName: "ReferralSummary" */ '@/components/user/ReferralSummary.vue'),
      },
      {
      path: 'heat-map',
      name: 'HeatMap',
      component: () => import(/* webpackChunkName: "HeatMap" */ '@/components/user/HeatMap.vue'),
    },
    {
      path: 'user-summary',
      name: 'UserSummary',
      component: () => import(/* webpackChunkName: "UserSummery" */ '@/components/user/UserSummery.vue'),
    }

    ]
  },
  {
    path: '/task',
    name: 'Task',
    component: () => import(/* webpackChunkName: "Support" */ '@/views/Task.vue'),
    redirect: { name: 'TaskSendemail' },
    children: [
      {
        path: 'sendemail',
        name: 'TaskSendemail',
        component: () => import(/* webpackChunkName: "TaskSendemail" */ '@/components/task/TaskSendemail.vue'),
      },
      {
        path: 'gems',
        name: 'TaskGems',
        component: () => import(/* webpackChunkName: "TaskGems" */ '@/components/task/TaskGems.vue'),
      },
      {
      path: 'downloaddata',
      name: 'TaskDownloaddata',
      component: () => import(/* webpackChunkName: "TaskDownloaddata" */ '@/components/task/TaskDownloaddata.vue'),
    },
    {
      path: 'create',
      name: 'TaskCreate',
      component: () => import(/* webpackChunkName: "TaskCreate" */ '@/components/task/TaskCreate.vue'),
    }

    ]
  },
  {
    path: '/tournament',
    name: 'TournamentDetail',
    component: () => import(/* webpackChunkName: "TournamentSummary" */ '@/views/TournamentSummary.vue'),
    redirect: { name: 'TournamentSummary' },
    children: [
      {
        path: 'overall-summary',
        name: 'TournamentSummary',
        component: () => import(/* webpackChunkName: "OverallSummary" */ '@/components/tournament/OverallSummary.vue'),
      }

    ]
  },
  {
    path: '/leaderboard',
    name: 'LeaderboardDetail',
    component: () => import(/* webpackChunkName: "TournamentSummary" */ '@/views/LeaderboardSummary.vue'),
    redirect: { name: 'LeaderboardSummary' },
    children: [
      {
        path: 'overall-summary',
        name: 'LeaderboardSummary',
        component: () => import(/* webpackChunkName: "OverallSummary" */ '@/components/leaderboard/OverallSummary.vue'),
      }

    ]
  },
  {
    path: '/tournaments/:id',
    name: 'TournamentParticipants',
    component: () => import('@/views/TournamentParticipants.vue')
  },
  {
    path: '/tournaments/prize/:id',
    name: 'TournamentPrizes',
    component: () => import('@/views/TournamentPrizes.vue')
  },
  {
    path: '/leaderboard/:id',
    name: 'LeaderboardParticipants',
    component: () => import('@/views/LeaderboardParticipants.vue')
  },
  {
    path: '/leaderboard/prize/:id',
    name: 'LeaderboardPrizes',
    component: () => import('@/views/LeaderboardPrizes.vue')
  },
]

const router = new VueRouter({
  mode: 'hash',
  routes
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = store.state.login.user;
  // redirect to login page if user is not logged in and trying to access a restricted page
  if (authRequired && !loggedIn) {
    return next('/login');
  }
  // redirect to home page if user is logged in and trying to access login page
  if (loggedIn && to.path === '/login') {
    return next('/');
  }
  next();
})

export default router

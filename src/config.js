export const environment = Object.freeze({
    // apiEndPoint: process.env.VUE_APP_API_END_POINT,
    // apiEndPoint: 'http://localhost:3000/',
    // apiEndPoint: 'http://54.38.194.44/',
    apiEndPoint: 'https://365-api.indiancreed.com/'
})
export const ROLE = Object.freeze({
	ADMIN: 'admin',
	SUPER_ADMIN: 'superadmin',
	BACKEND_DEVELOPER: 'backenddeveloper'
})

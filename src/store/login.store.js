import axios from 'axios';
import { setUser, getUser, removeUser } from '../StorageFactory';

export const login = {
    state: () => ({
        user: getUser()
    }),
    mutations: {
        SET_USER_DATA (state, user) {
            setUser(user)
            axios.defaults.headers.common['Authorization'] = `Bearer ${ user.token }`
            state.user = user
        },
        LOGOUT () {
            removeUser()
            location.reload()
        },
    },
    actions: {
        async login ({ commit }, body) {
            const { data } = await axios.post('/admin/login', body);
            if (data) commit('SET_USER_DATA', data);
            return data;
        },
        logout ({ commit }) {
            commit('LOGOUT');
        },
    },
    getters: {
        loggedIn: state => !!state.user,
        user: state => state.user
    }
  }
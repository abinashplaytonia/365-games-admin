import axios from 'axios';

export const role = {
    state: () => ({
        role: null
    }),
    mutations: {
        SET_USER_ROLE (state, role) {
            state.role = role
        },
    },
    actions: {
        async getRole ({ commit }) {
            const { data } = await axios.get('/admin/get');
            if (data) commit('SET_USER_ROLE', data);
            return data;
        },
    },
    getters: {
        checkRoleAccess: (state) => (access) => {
            if(state.role?.name === 'superadmin') return true;
            return !!state.role?.accessList?.find(element => element.access === access)
          },
        role: state => state.role
    }
  }